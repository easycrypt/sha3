{ }:

with import <nixpkgs> {};

pkgs.mkShell {
  buildInputs = [ easycrypt easycrypt-runtest z3 cvc4 alt-ergo ];
}
