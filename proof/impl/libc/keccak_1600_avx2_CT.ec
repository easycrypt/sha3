require import List Int IntExtra IntDiv CoreMap.
from Jasmin require import JModel.

require import Array7 Array9 Array28.
require import WArray224 WArray288.

abbrev g_zero = W64.of_int 0.


module M = {
  var leakages : leakages_t
  
  proc __keccak_f1600_avx2 (state:W256.t Array7.t, _rhotates_left:W64.t,
                            _rhotates_right:W64.t, _iotas:W64.t) : W256.t Array7.t = {
    var aux_5: bool;
    var aux_4: bool;
    var aux_3: bool;
    var aux_2: bool;
    var aux_0: W32.t;
    var aux: W64.t;
    var aux_1: W256.t;
    
    var rhotates_left:W64.t;
    var rhotates_right:W64.t;
    var iotas:W64.t;
    var r:W32.t;
    var zf:bool;
    var c00:W256.t;
    var c14:W256.t;
    var t:W256.t Array9.t;
    var d14:W256.t;
    var d00:W256.t;
    var  _0:bool;
    var  _1:bool;
    var  _2:bool;
    t <- witness;
    leakages <- LeakAddr([]) :: leakages;
    aux <- (_rhotates_left + (W64.of_int 96));
    rhotates_left <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- (_rhotates_right + (W64.of_int 96));
    rhotates_right <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- _iotas;
    iotas <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (W32.of_int 24);
    r <- aux_0;
    leakages <- LeakAddr([2]) :: leakages;
    aux_1 <- x86_VPSHUFD_256 state.[2]
    (W8.of_int (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 1))));
    c00 <- aux_1;
    leakages <- LeakAddr([3; 5]) :: leakages;
    aux_1 <- (state.[5] `^` state.[3]);
    c14 <- aux_1;
    leakages <- LeakAddr([6; 4]) :: leakages;
    aux_1 <- (state.[4] `^` state.[6]);
    leakages <- LeakAddr([2]) :: leakages;
    t.[2] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- (c14 `^` state.[1]);
    c14 <- aux_1;
    leakages <- LeakAddr([2]) :: leakages;
    aux_1 <- (c14 `^` t.[2]);
    c14 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- x86_VPERMQ c14
    (W8.of_int (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 2))));
    leakages <- LeakAddr([4]) :: leakages;
    t.[4] <- aux_1;
    leakages <- LeakAddr([2]) :: leakages;
    aux_1 <- (c00 `^` state.[2]);
    c00 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- x86_VPERMQ c00
    (W8.of_int (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 1))));
    leakages <- LeakAddr([0]) :: leakages;
    t.[0] <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (c14 \vshr64u256 (W8.of_int 63));
    leakages <- LeakAddr([1]) :: leakages;
    t.[1] <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (c14 \vadd64u256 c14);
    leakages <- LeakAddr([2]) :: leakages;
    t.[2] <- aux_1;
    leakages <- LeakAddr([2; 1]) :: leakages;
    aux_1 <- (t.[1] `|` t.[2]);
    leakages <- LeakAddr([1]) :: leakages;
    t.[1] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- x86_VPERMQ t.[1]
    (W8.of_int (1 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 0))));
    d14 <- aux_1;
    leakages <- LeakAddr([4; 1]) :: leakages;
    aux_1 <- (t.[1] `^` t.[4]);
    d00 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- x86_VPERMQ d00
    (W8.of_int (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 0))));
    d00 <- aux_1;
    leakages <- LeakAddr([0]) :: leakages;
    aux_1 <- (c00 `^` state.[0]);
    c00 <- aux_1;
    leakages <- LeakAddr([0]) :: leakages;
    aux_1 <- (c00 `^` t.[0]);
    c00 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (c00 \vshr64u256 (W8.of_int 63));
    leakages <- LeakAddr([0]) :: leakages;
    t.[0] <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (c00 \vadd64u256 c00);
    leakages <- LeakAddr([1]) :: leakages;
    t.[1] <- aux_1;
    leakages <- LeakAddr([0; 1]) :: leakages;
    aux_1 <- (t.[1] `|` t.[0]);
    leakages <- LeakAddr([1]) :: leakages;
    t.[1] <- aux_1;
    leakages <- LeakAddr([2]) :: leakages;
    aux_1 <- (state.[2] `^` d00);
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([0]) :: leakages;
    aux_1 <- (state.[0] `^` d00);
    leakages <- LeakAddr([0]) :: leakages;
    state.[0] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 d14 t.[1]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    d14 <- aux_1;
    leakages <- LeakAddr([4]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[4] c00
    (W8.of_int (1 %% 2^1 +
               2^1 * (1 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([4]) :: leakages;
    t.[4] <- aux_1;
    leakages <- LeakAddr([4]) :: leakages;
    aux_1 <- (d14 `^` t.[4]);
    d14 <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 0) - 96))));
                         2]) :: leakages;
    aux_1 <- x86_VPSLLV_4u64 state.[2]
    (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 0) - 96)))));
    leakages <- LeakAddr([3]) :: leakages;
    t.[3] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 0) - 96))));
                         2]) :: leakages;
    aux_1 <- x86_VPSRLV_4u64 state.[2]
    (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 0) - 96)))));
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([3; 2]) :: leakages;
    aux_1 <- (state.[2] `|` t.[3]);
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([3]) :: leakages;
    aux_1 <- (state.[3] `^` d14);
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 2) - 96))));
                         3]) :: leakages;
    aux_1 <- x86_VPSLLV_4u64 state.[3]
    (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 2) - 96)))));
    leakages <- LeakAddr([4]) :: leakages;
    t.[4] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 2) - 96))));
                         3]) :: leakages;
    aux_1 <- x86_VPSRLV_4u64 state.[3]
    (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 2) - 96)))));
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([4; 3]) :: leakages;
    aux_1 <- (state.[3] `|` t.[4]);
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([4]) :: leakages;
    aux_1 <- (state.[4] `^` d14);
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 3) - 96))));
                         4]) :: leakages;
    aux_1 <- x86_VPSLLV_4u64 state.[4]
    (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 3) - 96)))));
    leakages <- LeakAddr([5]) :: leakages;
    t.[5] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 3) - 96))));
                         4]) :: leakages;
    aux_1 <- x86_VPSRLV_4u64 state.[4]
    (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 3) - 96)))));
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([5; 4]) :: leakages;
    aux_1 <- (state.[4] `|` t.[5]);
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([5]) :: leakages;
    aux_1 <- (state.[5] `^` d14);
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 4) - 96))));
                         5]) :: leakages;
    aux_1 <- x86_VPSLLV_4u64 state.[5]
    (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 4) - 96)))));
    leakages <- LeakAddr([6]) :: leakages;
    t.[6] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 4) - 96))));
                         5]) :: leakages;
    aux_1 <- x86_VPSRLV_4u64 state.[5]
    (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 4) - 96)))));
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([6; 5]) :: leakages;
    aux_1 <- (state.[5] `|` t.[6]);
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([6]) :: leakages;
    aux_1 <- (state.[6] `^` d14);
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([2]) :: leakages;
    aux_1 <- x86_VPERMQ state.[2]
    (W8.of_int (1 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 2))));
    leakages <- LeakAddr([3]) :: leakages;
    t.[3] <- aux_1;
    leakages <- LeakAddr([3]) :: leakages;
    aux_1 <- x86_VPERMQ state.[3]
    (W8.of_int (1 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 2))));
    leakages <- LeakAddr([4]) :: leakages;
    t.[4] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 5) - 96))));
                         6]) :: leakages;
    aux_1 <- x86_VPSLLV_4u64 state.[6]
    (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 5) - 96)))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 5) - 96))));
                         6]) :: leakages;
    aux_1 <- x86_VPSRLV_4u64 state.[6]
    (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 5) - 96)))));
    leakages <- LeakAddr([1]) :: leakages;
    t.[1] <- aux_1;
    leakages <- LeakAddr([7; 1]) :: leakages;
    aux_1 <- (t.[1] `|` t.[7]);
    leakages <- LeakAddr([1]) :: leakages;
    t.[1] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- (state.[1] `^` d14);
    leakages <- LeakAddr([1]) :: leakages;
    state.[1] <- aux_1;
    leakages <- LeakAddr([4]) :: leakages;
    aux_1 <- x86_VPERMQ state.[4]
    (W8.of_int (3 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 0))));
    leakages <- LeakAddr([5]) :: leakages;
    t.[5] <- aux_1;
    leakages <- LeakAddr([5]) :: leakages;
    aux_1 <- x86_VPERMQ state.[5]
    (W8.of_int (2 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 1))));
    leakages <- LeakAddr([6]) :: leakages;
    t.[6] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 1) - 96))));
                         1]) :: leakages;
    aux_1 <- x86_VPSLLV_4u64 state.[1]
    (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 1) - 96)))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 1) - 96))));
                         1]) :: leakages;
    aux_1 <- x86_VPSRLV_4u64 state.[1]
    (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 1) - 96)))));
    leakages <- LeakAddr([2]) :: leakages;
    t.[2] <- aux_1;
    leakages <- LeakAddr([8; 2]) :: leakages;
    aux_1 <- (t.[2] `|` t.[8]);
    leakages <- LeakAddr([2]) :: leakages;
    t.[2] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- x86_VPSRLDQ_256 t.[1] (W8.of_int 8);
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([7; 1]) :: leakages;
    aux_1 <- ((invw t.[1]) `&` t.[7]);
    leakages <- LeakAddr([0]) :: leakages;
    t.[0] <- aux_1;
    leakages <- LeakAddr([6; 2]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[2] t.[6]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([2; 4]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[4] t.[2]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([4; 3]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[3] t.[4]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([3; 2]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[2] t.[3]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([4; 3]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[3] t.[4]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([5; 8]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[8] t.[5]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([2; 5]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[5] t.[2]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([6; 7]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[7] t.[6]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([5; 3]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[3] t.[5]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([6; 8]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[8] t.[6]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([6; 5]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[5] t.[6]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([4; 7]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[7] t.[4]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([8; 3]) :: leakages;
    aux_1 <- ((invw state.[3]) `&` t.[8]);
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([7; 5]) :: leakages;
    aux_1 <- ((invw state.[5]) `&` t.[7]);
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([2; 5]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[5] t.[2]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([5; 3]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[3] t.[5]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([3; 3]) :: leakages;
    aux_1 <- (state.[3] `^` t.[3]);
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([3; 6]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[6] t.[3]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([4; 8]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[8] t.[4]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([5; 5]) :: leakages;
    aux_1 <- (state.[5] `^` t.[5]);
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([4; 6]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[6] t.[4]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([2; 8]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[8] t.[2]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([8; 6]) :: leakages;
    aux_1 <- ((invw state.[6]) `&` t.[8]);
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([6; 6]) :: leakages;
    aux_1 <- (state.[6] `^` t.[6]);
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- x86_VPERMQ t.[1]
    (W8.of_int (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 0))));
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([0; 4]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[4] state.[0]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([8]) :: leakages;
    t.[8] <- aux_1;
    leakages <- LeakAddr([1]) :: leakages;
    aux_1 <- x86_VPERMQ t.[1]
    (W8.of_int (1 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 0))));
    leakages <- LeakAddr([1]) :: leakages;
    state.[1] <- aux_1;
    leakages <- LeakAddr([0; 1]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[1] state.[0]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([1]) :: leakages;
    state.[1] <- aux_1;
    leakages <- LeakAddr([8; 1]) :: leakages;
    aux_1 <- ((invw state.[1]) `&` t.[8]);
    leakages <- LeakAddr([1]) :: leakages;
    state.[1] <- aux_1;
    leakages <- LeakAddr([5; 4]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[4] t.[5]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([4; 6]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[6] t.[4]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([6; 2]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[2] t.[6]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([3; 7]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[7] t.[3]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([3; 2]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[2] t.[3]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([5; 7]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[7] t.[5]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([7; 2]) :: leakages;
    aux_1 <- ((invw state.[2]) `&` t.[7]);
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([2; 2]) :: leakages;
    aux_1 <- (state.[2] `^` t.[2]);
    leakages <- LeakAddr([2]) :: leakages;
    state.[2] <- aux_1;
    leakages <- LeakAddr([0]) :: leakages;
    aux_1 <- x86_VPERMQ t.[0]
    (W8.of_int (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 0))));
    leakages <- LeakAddr([0]) :: leakages;
    t.[0] <- aux_1;
    leakages <- LeakAddr([3]) :: leakages;
    aux_1 <- x86_VPERMQ state.[3]
    (W8.of_int (3 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 0))));
    leakages <- LeakAddr([3]) :: leakages;
    state.[3] <- aux_1;
    leakages <- LeakAddr([5]) :: leakages;
    aux_1 <- x86_VPERMQ state.[5]
    (W8.of_int (1 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 2))));
    leakages <- LeakAddr([5]) :: leakages;
    state.[5] <- aux_1;
    leakages <- LeakAddr([6]) :: leakages;
    aux_1 <- x86_VPERMQ state.[6]
    (W8.of_int (2 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 1))));
    leakages <- LeakAddr([6]) :: leakages;
    state.[6] <- aux_1;
    leakages <- LeakAddr([3; 6]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[6] t.[3]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([6; 5]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[5] t.[6]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (1 %% 2^1 +
                           2^1 * (1 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([5; 4]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[4] t.[5]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([2; 7]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[7] t.[2]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (1 %% 2^1 +
                                       2^1 * (1 %% 2^1 +
                                             2^1 * (0 %% 2^1 + 2^1 * 0))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([2; 4]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 state.[4] t.[2]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([3; 7]) :: leakages;
    aux_1 <- x86_VPBLENDD_256 t.[7] t.[3]
    (W8.of_int (0 %% 2^1 +
               2^1 * (0 %% 2^1 +
                     2^1 * (0 %% 2^1 +
                           2^1 * (0 %% 2^1 +
                                 2^1 * (0 %% 2^1 +
                                       2^1 * (0 %% 2^1 +
                                             2^1 * (1 %% 2^1 + 2^1 * 1))))))));
    leakages <- LeakAddr([7]) :: leakages;
    t.[7] <- aux_1;
    leakages <- LeakAddr([7; 4]) :: leakages;
    aux_1 <- ((invw state.[4]) `&` t.[7]);
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([0; 0]) :: leakages;
    aux_1 <- (state.[0] `^` t.[0]);
    leakages <- LeakAddr([0]) :: leakages;
    state.[0] <- aux_1;
    leakages <- LeakAddr([1; 1]) :: leakages;
    aux_1 <- (state.[1] `^` t.[1]);
    leakages <- LeakAddr([1]) :: leakages;
    state.[1] <- aux_1;
    leakages <- LeakAddr([4; 4]) :: leakages;
    aux_1 <- (state.[4] `^` t.[4]);
    leakages <- LeakAddr([4]) :: leakages;
    state.[4] <- aux_1;
    leakages <- LeakAddr([(W64.to_uint (iotas + (W64.of_int ((32 * 0) - 0))));
                         0]) :: leakages;
    aux_1 <- (state.[0] `^` (loadW256 Glob.mem (W64.to_uint (iotas + (W64.of_int ((32 * 0) - 0))))));
    leakages <- LeakAddr([0]) :: leakages;
    state.[0] <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux <- (iotas + (W64.of_int 32));
    iotas <- aux;
    leakages <- LeakAddr([]) :: leakages;
    (aux_5, aux_4, aux_3, aux_2, aux_0) <- x86_DEC_32 r;
     _0 <- aux_5;
     _1 <- aux_4;
     _2 <- aux_3;
    zf <- aux_2;
    r <- aux_0;
    leakages <- LeakCond((! zf)) :: LeakAddr([]) :: leakages;
    
    while ((! zf)) {
      leakages <- LeakAddr([2]) :: leakages;
      aux_1 <- x86_VPSHUFD_256 state.[2]
      (W8.of_int (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 1))));
      c00 <- aux_1;
      leakages <- LeakAddr([3; 5]) :: leakages;
      aux_1 <- (state.[5] `^` state.[3]);
      c14 <- aux_1;
      leakages <- LeakAddr([6; 4]) :: leakages;
      aux_1 <- (state.[4] `^` state.[6]);
      leakages <- LeakAddr([2]) :: leakages;
      t.[2] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- (c14 `^` state.[1]);
      c14 <- aux_1;
      leakages <- LeakAddr([2]) :: leakages;
      aux_1 <- (c14 `^` t.[2]);
      c14 <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- x86_VPERMQ c14
      (W8.of_int (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 2))));
      leakages <- LeakAddr([4]) :: leakages;
      t.[4] <- aux_1;
      leakages <- LeakAddr([2]) :: leakages;
      aux_1 <- (c00 `^` state.[2]);
      c00 <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- x86_VPERMQ c00
      (W8.of_int (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 1))));
      leakages <- LeakAddr([0]) :: leakages;
      t.[0] <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (c14 \vshr64u256 (W8.of_int 63));
      leakages <- LeakAddr([1]) :: leakages;
      t.[1] <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (c14 \vadd64u256 c14);
      leakages <- LeakAddr([2]) :: leakages;
      t.[2] <- aux_1;
      leakages <- LeakAddr([2; 1]) :: leakages;
      aux_1 <- (t.[1] `|` t.[2]);
      leakages <- LeakAddr([1]) :: leakages;
      t.[1] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- x86_VPERMQ t.[1]
      (W8.of_int (1 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 0))));
      d14 <- aux_1;
      leakages <- LeakAddr([4; 1]) :: leakages;
      aux_1 <- (t.[1] `^` t.[4]);
      d00 <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- x86_VPERMQ d00
      (W8.of_int (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 0))));
      d00 <- aux_1;
      leakages <- LeakAddr([0]) :: leakages;
      aux_1 <- (c00 `^` state.[0]);
      c00 <- aux_1;
      leakages <- LeakAddr([0]) :: leakages;
      aux_1 <- (c00 `^` t.[0]);
      c00 <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (c00 \vshr64u256 (W8.of_int 63));
      leakages <- LeakAddr([0]) :: leakages;
      t.[0] <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (c00 \vadd64u256 c00);
      leakages <- LeakAddr([1]) :: leakages;
      t.[1] <- aux_1;
      leakages <- LeakAddr([0; 1]) :: leakages;
      aux_1 <- (t.[1] `|` t.[0]);
      leakages <- LeakAddr([1]) :: leakages;
      t.[1] <- aux_1;
      leakages <- LeakAddr([2]) :: leakages;
      aux_1 <- (state.[2] `^` d00);
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([0]) :: leakages;
      aux_1 <- (state.[0] `^` d00);
      leakages <- LeakAddr([0]) :: leakages;
      state.[0] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 d14 t.[1]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      d14 <- aux_1;
      leakages <- LeakAddr([4]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[4] c00
      (W8.of_int (1 %% 2^1 +
                 2^1 * (1 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([4]) :: leakages;
      t.[4] <- aux_1;
      leakages <- LeakAddr([4]) :: leakages;
      aux_1 <- (d14 `^` t.[4]);
      d14 <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 0) - 96))));
                           2]) :: leakages;
      aux_1 <- x86_VPSLLV_4u64 state.[2]
      (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 0) - 96)))));
      leakages <- LeakAddr([3]) :: leakages;
      t.[3] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 0) - 96))));
                           2]) :: leakages;
      aux_1 <- x86_VPSRLV_4u64 state.[2]
      (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 0) - 96)))));
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([3; 2]) :: leakages;
      aux_1 <- (state.[2] `|` t.[3]);
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([3]) :: leakages;
      aux_1 <- (state.[3] `^` d14);
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 2) - 96))));
                           3]) :: leakages;
      aux_1 <- x86_VPSLLV_4u64 state.[3]
      (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 2) - 96)))));
      leakages <- LeakAddr([4]) :: leakages;
      t.[4] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 2) - 96))));
                           3]) :: leakages;
      aux_1 <- x86_VPSRLV_4u64 state.[3]
      (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 2) - 96)))));
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([4; 3]) :: leakages;
      aux_1 <- (state.[3] `|` t.[4]);
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([4]) :: leakages;
      aux_1 <- (state.[4] `^` d14);
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 3) - 96))));
                           4]) :: leakages;
      aux_1 <- x86_VPSLLV_4u64 state.[4]
      (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 3) - 96)))));
      leakages <- LeakAddr([5]) :: leakages;
      t.[5] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 3) - 96))));
                           4]) :: leakages;
      aux_1 <- x86_VPSRLV_4u64 state.[4]
      (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 3) - 96)))));
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([5; 4]) :: leakages;
      aux_1 <- (state.[4] `|` t.[5]);
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([5]) :: leakages;
      aux_1 <- (state.[5] `^` d14);
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 4) - 96))));
                           5]) :: leakages;
      aux_1 <- x86_VPSLLV_4u64 state.[5]
      (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 4) - 96)))));
      leakages <- LeakAddr([6]) :: leakages;
      t.[6] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 4) - 96))));
                           5]) :: leakages;
      aux_1 <- x86_VPSRLV_4u64 state.[5]
      (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 4) - 96)))));
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([6; 5]) :: leakages;
      aux_1 <- (state.[5] `|` t.[6]);
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([6]) :: leakages;
      aux_1 <- (state.[6] `^` d14);
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([2]) :: leakages;
      aux_1 <- x86_VPERMQ state.[2]
      (W8.of_int (1 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 2))));
      leakages <- LeakAddr([3]) :: leakages;
      t.[3] <- aux_1;
      leakages <- LeakAddr([3]) :: leakages;
      aux_1 <- x86_VPERMQ state.[3]
      (W8.of_int (1 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 2))));
      leakages <- LeakAddr([4]) :: leakages;
      t.[4] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 5) - 96))));
                           6]) :: leakages;
      aux_1 <- x86_VPSLLV_4u64 state.[6]
      (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 5) - 96)))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 5) - 96))));
                           6]) :: leakages;
      aux_1 <- x86_VPSRLV_4u64 state.[6]
      (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 5) - 96)))));
      leakages <- LeakAddr([1]) :: leakages;
      t.[1] <- aux_1;
      leakages <- LeakAddr([7; 1]) :: leakages;
      aux_1 <- (t.[1] `|` t.[7]);
      leakages <- LeakAddr([1]) :: leakages;
      t.[1] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- (state.[1] `^` d14);
      leakages <- LeakAddr([1]) :: leakages;
      state.[1] <- aux_1;
      leakages <- LeakAddr([4]) :: leakages;
      aux_1 <- x86_VPERMQ state.[4]
      (W8.of_int (3 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 0))));
      leakages <- LeakAddr([5]) :: leakages;
      t.[5] <- aux_1;
      leakages <- LeakAddr([5]) :: leakages;
      aux_1 <- x86_VPERMQ state.[5]
      (W8.of_int (2 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 1))));
      leakages <- LeakAddr([6]) :: leakages;
      t.[6] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_left + (W64.of_int ((32 * 1) - 96))));
                           1]) :: leakages;
      aux_1 <- x86_VPSLLV_4u64 state.[1]
      (loadW256 Glob.mem (W64.to_uint (rhotates_left + (W64.of_int ((32 * 1) - 96)))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (rhotates_right + (W64.of_int ((32 * 1) - 96))));
                           1]) :: leakages;
      aux_1 <- x86_VPSRLV_4u64 state.[1]
      (loadW256 Glob.mem (W64.to_uint (rhotates_right + (W64.of_int ((32 * 1) - 96)))));
      leakages <- LeakAddr([2]) :: leakages;
      t.[2] <- aux_1;
      leakages <- LeakAddr([8; 2]) :: leakages;
      aux_1 <- (t.[2] `|` t.[8]);
      leakages <- LeakAddr([2]) :: leakages;
      t.[2] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- x86_VPSRLDQ_256 t.[1] (W8.of_int 8);
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([7; 1]) :: leakages;
      aux_1 <- ((invw t.[1]) `&` t.[7]);
      leakages <- LeakAddr([0]) :: leakages;
      t.[0] <- aux_1;
      leakages <- LeakAddr([6; 2]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[2] t.[6]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([2; 4]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[4] t.[2]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([4; 3]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[3] t.[4]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([3; 2]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[2] t.[3]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([4; 3]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[3] t.[4]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([5; 8]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[8] t.[5]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([2; 5]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[5] t.[2]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([6; 7]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[7] t.[6]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([5; 3]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[3] t.[5]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([6; 8]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[8] t.[6]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([6; 5]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[5] t.[6]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([4; 7]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[7] t.[4]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([8; 3]) :: leakages;
      aux_1 <- ((invw state.[3]) `&` t.[8]);
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([7; 5]) :: leakages;
      aux_1 <- ((invw state.[5]) `&` t.[7]);
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([2; 5]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[5] t.[2]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([5; 3]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[3] t.[5]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([3; 3]) :: leakages;
      aux_1 <- (state.[3] `^` t.[3]);
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([3; 6]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[6] t.[3]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([4; 8]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[8] t.[4]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([5; 5]) :: leakages;
      aux_1 <- (state.[5] `^` t.[5]);
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([4; 6]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[6] t.[4]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([2; 8]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[8] t.[2]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([8; 6]) :: leakages;
      aux_1 <- ((invw state.[6]) `&` t.[8]);
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([6; 6]) :: leakages;
      aux_1 <- (state.[6] `^` t.[6]);
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- x86_VPERMQ t.[1]
      (W8.of_int (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 0))));
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([0; 4]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[4] state.[0]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([8]) :: leakages;
      t.[8] <- aux_1;
      leakages <- LeakAddr([1]) :: leakages;
      aux_1 <- x86_VPERMQ t.[1]
      (W8.of_int (1 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 0))));
      leakages <- LeakAddr([1]) :: leakages;
      state.[1] <- aux_1;
      leakages <- LeakAddr([0; 1]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[1] state.[0]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([1]) :: leakages;
      state.[1] <- aux_1;
      leakages <- LeakAddr([8; 1]) :: leakages;
      aux_1 <- ((invw state.[1]) `&` t.[8]);
      leakages <- LeakAddr([1]) :: leakages;
      state.[1] <- aux_1;
      leakages <- LeakAddr([5; 4]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[4] t.[5]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([4; 6]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[6] t.[4]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([6; 2]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[2] t.[6]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([3; 7]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[7] t.[3]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([3; 2]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[2] t.[3]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([5; 7]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[7] t.[5]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([7; 2]) :: leakages;
      aux_1 <- ((invw state.[2]) `&` t.[7]);
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([2; 2]) :: leakages;
      aux_1 <- (state.[2] `^` t.[2]);
      leakages <- LeakAddr([2]) :: leakages;
      state.[2] <- aux_1;
      leakages <- LeakAddr([0]) :: leakages;
      aux_1 <- x86_VPERMQ t.[0]
      (W8.of_int (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 0))));
      leakages <- LeakAddr([0]) :: leakages;
      t.[0] <- aux_1;
      leakages <- LeakAddr([3]) :: leakages;
      aux_1 <- x86_VPERMQ state.[3]
      (W8.of_int (3 %% 2^2 + 2^2 * (2 %% 2^2 + 2^2 * (1 %% 2^2 + 2^2 * 0))));
      leakages <- LeakAddr([3]) :: leakages;
      state.[3] <- aux_1;
      leakages <- LeakAddr([5]) :: leakages;
      aux_1 <- x86_VPERMQ state.[5]
      (W8.of_int (1 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * 2))));
      leakages <- LeakAddr([5]) :: leakages;
      state.[5] <- aux_1;
      leakages <- LeakAddr([6]) :: leakages;
      aux_1 <- x86_VPERMQ state.[6]
      (W8.of_int (2 %% 2^2 + 2^2 * (0 %% 2^2 + 2^2 * (3 %% 2^2 + 2^2 * 1))));
      leakages <- LeakAddr([6]) :: leakages;
      state.[6] <- aux_1;
      leakages <- LeakAddr([3; 6]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[6] t.[3]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([6; 5]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[5] t.[6]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (1 %% 2^1 +
                             2^1 * (1 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([5; 4]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[4] t.[5]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([2; 7]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[7] t.[2]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (1 %% 2^1 +
                                         2^1 * (1 %% 2^1 +
                                               2^1 * (0 %% 2^1 + 2^1 * 0))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([2; 4]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 state.[4] t.[2]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([3; 7]) :: leakages;
      aux_1 <- x86_VPBLENDD_256 t.[7] t.[3]
      (W8.of_int (0 %% 2^1 +
                 2^1 * (0 %% 2^1 +
                       2^1 * (0 %% 2^1 +
                             2^1 * (0 %% 2^1 +
                                   2^1 * (0 %% 2^1 +
                                         2^1 * (0 %% 2^1 +
                                               2^1 * (1 %% 2^1 + 2^1 * 1))))))));
      leakages <- LeakAddr([7]) :: leakages;
      t.[7] <- aux_1;
      leakages <- LeakAddr([7; 4]) :: leakages;
      aux_1 <- ((invw state.[4]) `&` t.[7]);
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([0; 0]) :: leakages;
      aux_1 <- (state.[0] `^` t.[0]);
      leakages <- LeakAddr([0]) :: leakages;
      state.[0] <- aux_1;
      leakages <- LeakAddr([1; 1]) :: leakages;
      aux_1 <- (state.[1] `^` t.[1]);
      leakages <- LeakAddr([1]) :: leakages;
      state.[1] <- aux_1;
      leakages <- LeakAddr([4; 4]) :: leakages;
      aux_1 <- (state.[4] `^` t.[4]);
      leakages <- LeakAddr([4]) :: leakages;
      state.[4] <- aux_1;
      leakages <- LeakAddr([(W64.to_uint (iotas + (W64.of_int ((32 * 0) - 0))));
                           0]) :: leakages;
      aux_1 <- (state.[0] `^` (loadW256 Glob.mem (W64.to_uint (iotas + (W64.of_int ((32 * 0) - 0))))));
      leakages <- LeakAddr([0]) :: leakages;
      state.[0] <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux <- (iotas + (W64.of_int 32));
      iotas <- aux;
      leakages <- LeakAddr([]) :: leakages;
      (aux_5, aux_4, aux_3, aux_2, aux_0) <- x86_DEC_32 r;
       _0 <- aux_5;
       _1 <- aux_4;
       _2 <- aux_3;
      zf <- aux_2;
      r <- aux_0;
    leakages <- LeakCond((! zf)) :: LeakAddr([]) :: leakages;
    
    }
    return (state);
  }
  
  proc keccak_init () : W256.t Array7.t = {
    var aux_0: int;
    var aux: W256.t;
    
    var state:W256.t Array7.t;
    var i:int;
    state <- witness;
    leakages <- LeakAddr([]) :: leakages;
    aux <- x86_VPBROADCAST_4u64 g_zero;
    leakages <- LeakAddr([0]) :: leakages;
    state.[0] <- aux;
    leakages <- LeakFor(1,7) :: LeakAddr([]) :: leakages;
    i <- 1;
    while (i < 7) {
      leakages <- LeakAddr([0]) :: leakages;
      aux <- state.[0];
      leakages <- LeakAddr([i]) :: leakages;
      state.[i] <- aux;
      i <- i + 1;
    }
    return (state);
  }
  
  proc init_s_state () : W64.t Array28.t = {
    var aux_0: int;
    var aux: W256.t;
    
    var s_state:W64.t Array28.t;
    var zero:W256.t;
    var i:int;
    s_state <- witness;
    leakages <- LeakAddr([]) :: leakages;
    aux <- x86_VPBROADCAST_4u64 g_zero;
    zero <- aux;
    leakages <- LeakFor(0,7) :: LeakAddr([]) :: leakages;
    i <- 0;
    while (i < 7) {
      leakages <- LeakAddr([]) :: leakages;
      aux <- zero;
      leakages <- LeakAddr([i]) :: leakages;
      s_state =
      Array28.init
      (WArray224.get64 (WArray224.set256 (WArray224.init64 (fun i => s_state.[i])) i aux));
      i <- i + 1;
    }
    return (s_state);
  }
  
  proc add_full_block (state:W256.t Array7.t, s_state:W64.t Array28.t,
                       a_jagged:W64.t, in_0:W64.t, inlen:W64.t, rate:W64.t) : 
  W256.t Array7.t * W64.t Array28.t * W64.t * W64.t = {
    var aux_0: int;
    var aux: W64.t;
    var aux_1: W256.t;
    
    var rate8:W64.t;
    var j:W64.t;
    var t:W64.t;
    var l:W64.t;
    var i:int;
    
    leakages <- LeakAddr([]) :: leakages;
    aux <- rate;
    rate8 <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- (rate8 `>>` (W8.of_int 3));
    rate8 <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- (W64.of_int 0);
    j <- aux;
    
    leakages <- LeakCond((j \ult rate8)) :: LeakAddr([]) :: leakages;
    
    while ((j \ult rate8)) {
      leakages <- LeakAddr([(W64.to_uint (in_0 + ((W64.of_int 8) * j)))]) :: leakages;
      aux <- (loadW64 Glob.mem (W64.to_uint (in_0 + ((W64.of_int 8) * j))));
      t <- aux;
      leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
      aux <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
      l <- aux;
      leakages <- LeakAddr([]) :: leakages;
      aux <- t;
      leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
      s_state.[(W64.to_uint l)] <- aux;
      leakages <- LeakAddr([]) :: leakages;
      aux <- (j + (W64.of_int 1));
      j <- aux;
    leakages <- LeakCond((j \ult rate8)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([0]) :: leakages;
    aux <- s_state.[0];
    t <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- t;
    leakages <- LeakAddr([1]) :: leakages;
    s_state.[1] <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- t;
    leakages <- LeakAddr([2]) :: leakages;
    s_state.[2] <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- t;
    leakages <- LeakAddr([3]) :: leakages;
    s_state.[3] <- aux;
    leakages <- LeakFor(0,7) :: LeakAddr([]) :: leakages;
    i <- 0;
    while (i < 7) {
      leakages <- LeakAddr([i; i]) :: leakages;
      aux_1 <- (state.[i] `^` (get256
                              (WArray224.init64 (fun i => s_state.[i])) i));
      leakages <- LeakAddr([i]) :: leakages;
      state.[i] <- aux_1;
      i <- i + 1;
    }
    leakages <- LeakAddr([]) :: leakages;
    aux <- (in_0 + rate);
    in_0 <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <- (inlen - rate);
    inlen <- aux;
    return (state, s_state, in_0, inlen);
  }
  
  proc add_final_block (state:W256.t Array7.t, s_state:W64.t Array28.t,
                        a_jagged:W64.t, in_0:W64.t, inlen:W64.t,
                        trail_byte:W8.t, rate:W64.t) : W256.t Array7.t = {
    var aux_2: int;
    var aux_1: W8.t;
    var aux_0: W64.t;
    var aux_3: W256.t;
    var aux: W64.t Array28.t;
    
    var inlen8:W64.t;
    var j:W64.t;
    var t:W64.t;
    var l:W64.t;
    var c:W8.t;
    var i:int;
    
    leakages <- LeakAddr([]) :: leakages;
    aux <@ init_s_state ();
    s_state <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- inlen;
    inlen8 <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (inlen8 `>>` (W8.of_int 3));
    inlen8 <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (W64.of_int 0);
    j <- aux_0;
    
    leakages <- LeakCond((j \ult inlen8)) :: LeakAddr([]) :: leakages;
    
    while ((j \ult inlen8)) {
      leakages <- LeakAddr([(W64.to_uint (in_0 + ((W64.of_int 8) * j)))]) :: leakages;
      aux_0 <- (loadW64 Glob.mem (W64.to_uint (in_0 + ((W64.of_int 8) * j))));
      t <- aux_0;
      leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
      aux_0 <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
      l <- aux_0;
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <- t;
      leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
      s_state.[(W64.to_uint l)] <- aux_0;
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <- (j + (W64.of_int 1));
      j <- aux_0;
    leakages <- LeakCond((j \ult inlen8)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
    aux_0 <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
    l <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (l `<<` (W8.of_int 3));
    l <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (j `<<` (W8.of_int 3));
    j <- aux_0;
    
    leakages <- LeakCond((j \ult inlen)) :: LeakAddr([]) :: leakages;
    
    while ((j \ult inlen)) {
      leakages <- LeakAddr([(W64.to_uint (in_0 + j))]) :: leakages;
      aux_1 <- (loadW8 Glob.mem (W64.to_uint (in_0 + j)));
      c <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- c;
      leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
      s_state =
      Array28.init
      (WArray224.get64 (WArray224.set8 (WArray224.init64 (fun i => s_state.[i])) (W64.to_uint l) aux_1));
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <- (j + (W64.of_int 1));
      j <- aux_0;
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <- (l + (W64.of_int 1));
      l <- aux_0;
    leakages <- LeakCond((j \ult inlen)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- trail_byte;
    leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
    s_state =
    Array28.init
    (WArray224.get64 (WArray224.set8 (WArray224.init64 (fun i => s_state.[i])) (W64.to_uint l) aux_1));
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- rate;
    j <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (j - (W64.of_int 1));
    j <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (j `>>` (W8.of_int 3));
    j <- aux_0;
    leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
    aux_0 <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
    l <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (l `<<` (W8.of_int 3));
    l <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- rate;
    j <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (j - (W64.of_int 1));
    j <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (j `&` (W64.of_int 7));
    j <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- (l + j);
    l <- aux_0;
    leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
    aux_1 <- ((get8 (WArray224.init64 (fun i => s_state.[i]))
              (W64.to_uint l)) `^` (W8.of_int 128));
    leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
    s_state =
    Array28.init
    (WArray224.get64 (WArray224.set8 (WArray224.init64 (fun i => s_state.[i])) (W64.to_uint l) aux_1));
    leakages <- LeakAddr([0]) :: leakages;
    aux_0 <- s_state.[0];
    t <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- t;
    leakages <- LeakAddr([1]) :: leakages;
    s_state.[1] <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- t;
    leakages <- LeakAddr([2]) :: leakages;
    s_state.[2] <- aux_0;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <- t;
    leakages <- LeakAddr([3]) :: leakages;
    s_state.[3] <- aux_0;
    leakages <- LeakFor(0,7) :: LeakAddr([]) :: leakages;
    i <- 0;
    while (i < 7) {
      leakages <- LeakAddr([i; i]) :: leakages;
      aux_3 <- (state.[i] `^` (get256
                              (WArray224.init64 (fun i => s_state.[i])) i));
      leakages <- LeakAddr([i]) :: leakages;
      state.[i] <- aux_3;
      i <- i + 1;
    }
    return (state);
  }
  
  proc xtr_full_block (state:W256.t Array7.t, a_jagged:W64.t, out:W64.t,
                       len:W64.t) : W64.t = {
    var aux: int;
    var aux_1: W64.t;
    var aux_0: W256.t;
    
    var i:int;
    var s_state:W64.t Array28.t;
    var len8:W64.t;
    var j:W64.t;
    var l:W64.t;
    var t:W64.t;
    s_state <- witness;
    leakages <- LeakFor(0,7) :: LeakAddr([]) :: leakages;
    i <- 0;
    while (i < 7) {
      leakages <- LeakAddr([i]) :: leakages;
      aux_0 <- state.[i];
      leakages <- LeakAddr([i]) :: leakages;
      s_state =
      Array28.init
      (WArray224.get64 (WArray224.set256 (WArray224.init64 (fun i => s_state.[i])) i aux_0));
      i <- i + 1;
    }
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- len;
    len8 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (len8 `>>` (W8.of_int 3));
    len8 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (W64.of_int 0);
    j <- aux_1;
    
    leakages <- LeakCond((j \ult len8)) :: LeakAddr([]) :: leakages;
    
    while ((j \ult len8)) {
      leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
      aux_1 <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
      l <- aux_1;
      leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
      aux_1 <- s_state.[(W64.to_uint l)];
      t <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- t;
      leakages <- LeakAddr([(W64.to_uint (out + ((W64.of_int 8) * j)))]) :: leakages;
      Glob.mem <-
      storeW64 Glob.mem (W64.to_uint (out + ((W64.of_int 8) * j))) aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (j + (W64.of_int 1));
      j <- aux_1;
    leakages <- LeakCond((j \ult len8)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (out + len);
    out <- aux_1;
    return (out);
  }
  
  proc xtr_bytes (state:W256.t Array7.t, a_jagged:W64.t, out:W64.t, len:W64.t) : 
  W64.t = {
    var aux: int;
    var aux_2: W8.t;
    var aux_1: W64.t;
    var aux_0: W256.t;
    
    var i:int;
    var s_state:W64.t Array28.t;
    var len8:W64.t;
    var j:W64.t;
    var l:W64.t;
    var t:W64.t;
    var c:W8.t;
    s_state <- witness;
    leakages <- LeakFor(0,7) :: LeakAddr([]) :: leakages;
    i <- 0;
    while (i < 7) {
      leakages <- LeakAddr([i]) :: leakages;
      aux_0 <- state.[i];
      leakages <- LeakAddr([i]) :: leakages;
      s_state =
      Array28.init
      (WArray224.get64 (WArray224.set256 (WArray224.init64 (fun i => s_state.[i])) i aux_0));
      i <- i + 1;
    }
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- len;
    len8 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (len8 `>>` (W8.of_int 3));
    len8 <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (W64.of_int 0);
    j <- aux_1;
    
    leakages <- LeakCond((j \ult len8)) :: LeakAddr([]) :: leakages;
    
    while ((j \ult len8)) {
      leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
      aux_1 <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
      l <- aux_1;
      leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
      aux_1 <- s_state.[(W64.to_uint l)];
      t <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- t;
      leakages <- LeakAddr([(W64.to_uint (out + ((W64.of_int 8) * j)))]) :: leakages;
      Glob.mem <-
      storeW64 Glob.mem (W64.to_uint (out + ((W64.of_int 8) * j))) aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (j + (W64.of_int 1));
      j <- aux_1;
    leakages <- LeakCond((j \ult len8)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([(W64.to_uint (a_jagged + ((W64.of_int 8) * j)))]) :: leakages;
    aux_1 <- (loadW64 Glob.mem (W64.to_uint (a_jagged + ((W64.of_int 8) * j))));
    l <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (j `<<` (W8.of_int 3));
    j <- aux_1;
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (l `<<` (W8.of_int 3));
    l <- aux_1;
    
    leakages <- LeakCond((j \ult len)) :: LeakAddr([]) :: leakages;
    
    while ((j \ult len)) {
      leakages <- LeakAddr([(W64.to_uint l)]) :: leakages;
      aux_2 <- (get8 (WArray224.init64 (fun i => s_state.[i]))
               (W64.to_uint l));
      c <- aux_2;
      leakages <- LeakAddr([]) :: leakages;
      aux_2 <- c;
      leakages <- LeakAddr([(W64.to_uint (out + j))]) :: leakages;
      Glob.mem <- storeW8 Glob.mem (W64.to_uint (out + j)) aux_2;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (j + (W64.of_int 1));
      j <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_1 <- (l + (W64.of_int 1));
      l <- aux_1;
    leakages <- LeakCond((j \ult len)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([]) :: leakages;
    aux_1 <- (out + len);
    out <- aux_1;
    return (out);
  }
  
  proc absorb (state:W256.t Array7.t, rhotates_left:W64.t,
               rhotates_right:W64.t, iotas:W64.t, a_jagged:W64.t, in_0:W64.t,
               inlen:W64.t, trail_byte:W8.t, rate:W64.t) : W256.t Array7.t = {
    var aux_2: W64.t;
    var aux_1: W64.t;
    var aux: W64.t Array28.t;
    var aux_0: W256.t Array7.t;
    
    var s_state:W64.t Array28.t;
    s_state <- witness;
    leakages <- LeakAddr([]) :: leakages;
    aux <@ init_s_state ();
    s_state <- aux;
    
    leakages <- LeakCond((rate \ule inlen)) :: LeakAddr([]) :: leakages;
    
    while ((rate \ule inlen)) {
      leakages <- LeakAddr([]) :: leakages;
      (aux_0, aux, aux_2, aux_1) <@ add_full_block (state, s_state, a_jagged,
      in_0, inlen, rate);
      state <- aux_0;
      s_state <- aux;
      in_0 <- aux_2;
      inlen <- aux_1;
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <@ __keccak_f1600_avx2 (state, rhotates_left, rhotates_right,
      iotas);
      state <- aux_0;
    leakages <- LeakCond((rate \ule inlen)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <@ add_final_block (state, s_state, a_jagged, in_0, inlen,
    trail_byte, rate);
    state <- aux_0;
    return (state);
  }
  
  proc squeeze (state:W256.t Array7.t, rhotates_left:W64.t,
                rhotates_right:W64.t, iotas:W64.t, a_jagged:W64.t, out:W64.t,
                outlen:W64.t, rate:W64.t) : unit = {
    var aux_0: W64.t;
    var aux: W256.t Array7.t;
    
    
    
    
    leakages <- LeakCond((rate \ult outlen)) :: LeakAddr([]) :: leakages;
    
    while ((rate \ult outlen)) {
      leakages <- LeakAddr([]) :: leakages;
      aux <@ __keccak_f1600_avx2 (state, rhotates_left, rhotates_right,
      iotas);
      state <- aux;
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <@ xtr_full_block (state, a_jagged, out, rate);
      out <- aux_0;
      leakages <- LeakAddr([]) :: leakages;
      aux_0 <- (outlen - rate);
      outlen <- aux_0;
    leakages <- LeakCond((rate \ult outlen)) :: LeakAddr([]) :: leakages;
    
    }
    leakages <- LeakAddr([]) :: leakages;
    aux <@ __keccak_f1600_avx2 (state, rhotates_left, rhotates_right, iotas);
    state <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux_0 <@ xtr_bytes (state, a_jagged, out, outlen);
    out <- aux_0;
    return ();
  }
  
  proc __keccak_1600 (out:W64.t, outlen:W64.t, rhotates_left:W64.t,
                      rhotates_right:W64.t, iotas:W64.t, a_jagged:W64.t,
                      in_0:W64.t, inlen:W64.t, trail_byte:W8.t, rate:W64.t) : unit = {
    var aux: W256.t Array7.t;
    
    var state:W256.t Array7.t;
    state <- witness;
    leakages <- LeakAddr([]) :: leakages;
    aux <@ keccak_init ();
    state <- aux;
    leakages <- LeakAddr([]) :: leakages;
    aux <@ absorb (state, rhotates_left, rhotates_right, iotas, a_jagged,
    in_0, inlen, trail_byte, rate);
    state <- aux;
    leakages <- LeakAddr([]) :: leakages;
    squeeze (state, rhotates_left, rhotates_right, iotas, a_jagged, out,
    outlen, rate);
    return ();
  }
}.

op eq_reg (mem1 mem2 : global_mem_t, ptr len : int) =
    forall i, ptr <= i <= ptr + len - 8 =>
      loadW64 mem1 i = loadW64 mem2 i.

op disj_reg(ptr1 len1 ptr2 len2 : W64.t) =
  (to_uint (ptr1 + len1) < to_uint ptr2 \/ to_uint (ptr2 + len2) < to_uint ptr1).

lemma mem_sep (mem : global_mem_t) (ptr1 o1 x : W64.t) (ptr2 : int):
   0 <= ptr2 < W64.modulus - 8 /\
   to_uint ptr1 + to_uint o1 + 8 < W64.modulus /\
   disj_reg (ptr1 + o1) (W64.of_int 8) (W64.of_int ptr2) (W64.of_int 8) => 
  loadW64 (storeW64 mem (to_uint (ptr1 + o1)) x) ptr2 = 
       loadW64 mem ptr2.
progress.
rewrite storeW64E.
rewrite /loadW64. 
congr.
apply W8u8.Pack.init_ext.
move => x0 hxc0.
beta.
rewrite (get_storesE mem (to_uint (ptr1 + o1)) (((to_list x))%W8u8) (ptr2 + x0)).
rewrite  (_: to_uint (ptr1 + o1) <= ptr2 + x0 <
    to_uint (ptr1 + o1) + size ((to_list x))%W8u8 = false) => //=.
have bound : ( ptr2 + x0 < to_uint (ptr1 + o1) \/ to_uint (ptr1 + o1) + 8 <=ptr2 + x0); last by smt().
elim H2.
rewrite !to_uintD_small. smt(@W64).  smt(@W64). smt(@W64). 
rewrite !of_uintK => />.
smt(@W64).
rewrite !to_uintD_small. smt(@W64). smt(@W64).
progress.
move : H2. rewrite !of_uintK => />.
smt(@W64).
qed.

equiv ct :
  M.__keccak_1600 ~ M.__keccak_1600 :
     ={M.leakages,out,outlen,rhotates_left,rhotates_right,iotas,a_jagged,in_0,inlen,rate} /\ 
    to_uint out{2} + to_uint (outlen{2} + W64.of_int 8)  < W64.modulus /\
    to_uint a_jagged{2} + 224 < W64.modulus /\ 0 < to_uint rate{2} < 200 /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224  /\
    disj_reg out{1} (outlen{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
    disj_reg out{2} (outlen{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224) /\
    to_uint outlen{2} + 8 < W64.modulus ==> ={M.leakages}.
proc.
call (_: ={rhotates_left,rhotates_right,iotas,a_jagged,out,outlen,rate,M.leakages} /\        to_uint rate{2} < 200 /\  
    to_uint out{2} + to_uint (outlen{2} + W64.of_int 8) < W64.modulus /\
    to_uint a_jagged{2} + 224 < W64.modulus /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224  /\
    disj_reg out{1} (outlen{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
    disj_reg out{2} (outlen{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224)  /\
    to_uint outlen{2} + 8 < W64.modulus ==> ={M.leakages}).
proc.
wp; call(_: ={a_jagged,out,len,M.leakages} /\
        to_uint len{2} <= 200 /\
        to_uint out{2} + to_uint (len{2} + W64.of_int 8) < W64.modulus /\
        to_uint a_jagged{2} + 224 < W64.modulus /\
        eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224 /\ 
        disj_reg out{1} (len{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
        disj_reg out{2} (len{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224)  ==> ={M.leakages}).
proc.
wp; while (={j,len,M.leakages,out,l}).
by auto => />. 
wp; while (={j,M.leakages,out,a_jagged,len8,len} /\
    to_uint len8{2} <= 25 /\ to_uint j{2} <= to_uint len8{2} /\
    to_uint out{2} + to_uint (len{2} + W64.of_int 8) < W64.modulus /\
    to_uint a_jagged{2} + 224 < W64.modulus /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224  /\
    disj_reg out{1} (len{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
    disj_reg out{2} (len{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224) /\
    0 <= to_uint len8{2} < 26 /\ to_uint len8{1} = to_uint len{1} %/  8).
auto => />. 
rewrite /eq_reg /disj_reg. 
progress.
congr.
move : H8; rewrite ultE => *.
by apply H3;rewrite to_uintD_small;by smt(@W64).
by smt(@W64).
rewrite  (mem_sep Glob.mem{1} out{2}   
      (((of_int 8)%W64 * j{2})) 
        (s_state{1}.[to_uint
                   (loadW64 Glob.mem{1}
                      (to_uint (a_jagged{2} + (of_int 8)%W64 * j{2})))])). 
split.
smt(@W64). 
split.  
smt(@W64). 
rewrite /disj_reg.
elim H4.
rewrite !to_uintD_small => />. smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64).
move => H4.
left.
rewrite !to_uintM_small. smt(@W64).
rewrite of_uintK => />. 
move : H8; rewrite ultE.
rewrite H7.
move => *.
have hh : (8 * to_uint j{2} < to_uint len{2}). 
smt(@W64).
rewrite of_uintK => //=.
rewrite (_: i0 %% 18446744073709551616 = i0).
smt(@W64).
smt(@W64).

rewrite !to_uintD_small.
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).

rewrite  (mem_sep Glob.mem{2} out{2} 
      (((of_int 8)%W64 * j{2})) 
        (s_state{2}.[to_uint
                   (loadW64 Glob.mem{2}
                      (to_uint (a_jagged{2} + (of_int 8)%W64 * j{2})))])).
split.
smt(@W64). 
split.  
smt(@W64). 
rewrite /disj_reg.
elim H4.
rewrite !to_uintD_small => />. smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64).
move => H4.
left.
rewrite !to_uintM_small. smt(@W64).
rewrite of_uintK => />. 
move : H8; rewrite ultE.
rewrite H7.
move => *.
have hh : (8 * to_uint j{2} < to_uint len{2}). 
smt(@W64).
rewrite of_uintK => //=.
rewrite (_: i0 %% 18446744073709551616 = i0).
smt(@W64).
smt(@W64).

rewrite !to_uintD_small.
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).

apply H3. smt(@W64).
unroll for {1} 4.
unroll for {2} 4.
auto => />.
progress.
rewrite shr_div => />.
by smt().
rewrite shr_div => />.
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by rewrite shr_div => />.

move : H8; rewrite /eq_reg => *.
rewrite H8 =>//=.
by rewrite to_uintD_small; by smt(@W64).
wp;call(_: ={M.leakages,_rhotates_left,_rhotates_right,_iotas} ==> ={M.leakages}).
proc.
by sim.

wp;while(={M.leakages,rate,outlen,rhotates_left,rhotates_right,iotas,a_jagged,out} /\
    to_uint rate{2} < 200 /\ 
    to_uint out{2} + to_uint (outlen{2} + W64.of_int 8) < W64.modulus /\
    to_uint a_jagged{2} + 224 < W64.modulus /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224  /\
    disj_reg out{1} (outlen{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
    disj_reg out{2} (outlen{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224) /\
    to_uint outlen{2} + 8 < W64.modulus).

(* bug report, cannot use a_jagged for elim symbol *)
exists* a_jagged{1}, outlen{1}, rate{1}, out{1}.
elim* => a_j ol rt ot.
wp; call(_: ={a_jagged,out,len,M.leakages} /\ 
       to_uint len{2} < 200 /\ a_jagged{1} = a_j /\ len{1} = rt /\ out{1} = ot /\
    to_uint out{2} + to_uint (len{2} + W64.of_int 8) < W64.modulus /\
        to_uint a_jagged{2} + 224 < W64.modulus /\
        eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224 /\ 
        disj_reg out{1} (ol{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
        disj_reg out{2} (ol{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224) /\ len{1} \ult ol /\
       to_uint out{2} + (to_uint ol + 8) < W64.modulus
      ==> ={M.leakages,res} /\
           eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_j) 224 /\ 
              res{1} = ot + rt).
proc.
wp; while (={j,M.leakages,out,a_jagged,len8,len} /\ out{1} = ot /\
    a_jagged{1} = a_j /\ to_uint len8{1} = to_uint rt %/ 8 /\
    to_uint len8{2} < 25 /\ to_uint j{2} <= to_uint len8{2} /\
    to_uint out{2} + to_uint  (len{2} + W64.of_int 8)< W64.modulus /\
    to_uint a_jagged{2} + 224 < W64.modulus /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224  /\
    disj_reg out{1}  (ol{1} + W64.of_int 8) a_jagged{1} (W64.of_int 224) /\
    disj_reg out{2}  (ol{2} + W64.of_int 8) a_jagged{2} (W64.of_int 224) /\
    0 <= to_uint len8{2} < 26 /\ to_uint len8{1} = to_uint len{1} %/  8
    /\ len{1} \ult ol  /\ to_uint out{2} + (to_uint ol + 8) < W64.modulus).
auto => />. 
rewrite /eq_reg /disj_reg. 
progress.
congr.
move : H9; rewrite ultE => *.
by apply H4;rewrite to_uintD_small;  smt(@W64). 
by smt(@W64).
rewrite  (mem_sep Glob.mem{1} out{2}   
      (((of_int 8)%W64 * j{2})) 
        (s_state{1}.[to_uint
                   (loadW64 Glob.mem{1}
                      (to_uint (a_jagged{2} + (of_int 8)%W64 * j{2})))])). 
split.
smt(@W64). 
split.  
smt(@W64). 
rewrite /disj_reg.
elim H5.
rewrite !to_uintD_small => />. smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64).
move => H5.
left.
rewrite !to_uintM_small. smt(@W64).
rewrite of_uintK => />. 
move : H11; rewrite ultE.
rewrite H8.
move => *.
have hh : (8 * to_uint j{2} < to_uint len{2}). 
smt(@W64).
rewrite of_uintK => //=.
rewrite (_: i0 %% 18446744073709551616 = i0).
smt(@W64).
smt(@W64).

rewrite !to_uintD_small.
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).


rewrite  (mem_sep Glob.mem{2} out{2} 
      (((of_int 8)%W64 * j{2})) 
        (s_state{2}.[to_uint
                   (loadW64 Glob.mem{2}
                      (to_uint (a_jagged{2} + (of_int 8)%W64 * j{2})))])).
split.
smt(@W64). 
split.  
smt(@W64). 
rewrite /disj_reg.
elim H5.
rewrite !to_uintD_small => />. smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64). smt(@W64).
move => H5.
left.
rewrite !to_uintM_small. smt(@W64).
rewrite of_uintK => />. 
move : H11; rewrite ultE.
rewrite H8.
move => *.
have hh : (8 * to_uint j{2} < to_uint len{2}). 
smt(@W64).
rewrite of_uintK => //=.
rewrite (_: i0 %% 18446744073709551616 = i0).
smt(@W64).
smt(@W64).

rewrite !to_uintD_small.
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).

apply H4. smt(@W64).
unroll for {1} 4.
unroll for {2} 4.
auto => />.
progress.
by rewrite shr_div => />.
rewrite shr_div => />.
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by rewrite shr_div => />.

wp;call(_: ={M.leakages,_rhotates_left,_rhotates_right,_iotas} ==> ={M.leakages}).
proc.
by sim.

auto => />.
progress.
move : H5; rewrite ultE => *.
move : H0.
rewrite !to_uintD_small => />. smt(@W64). smt(@W64). 
move : H5; rewrite ultE => *.
move : H0.
rewrite !to_uintD_small => />. smt(@W64).  

move : H3; rewrite /disj_reg => *.
elim H3.
move => *.
left.
rewrite  (_: (out{2} + rate{2} + (outlen{2} - rate{2} + (of_int 8)%W64)) =
            (out{2} + outlen{2} + (of_int 8)%W64)). by ring.
smt(@W64).
move => *.
right.
rewrite (_: to_uint (out{2} + rate{2}) = to_uint out{2} + to_uint rate{2}).
rewrite to_uintD_small. smt(@W64).
smt(@W64).
smt(@W64).

move : H3; rewrite /disj_reg => *.
elim H3.
move => *.
left.
rewrite  (_: (out{2} + rate{2} + (outlen{2} - rate{2} + (of_int 8)%W64)) =
            (out{2} + outlen{2} + (of_int 8)%W64)). by ring.
smt(@W64).
move => *.
right.
rewrite (_: to_uint (out{2} + rate{2}) = to_uint out{2} + to_uint rate{2}).
rewrite to_uintD_small. smt(@W64).
smt(@W64).
smt(@W64).
smt(@W64).

auto => />.
progress.
smt(@W64).

wp;call(_: ={rhotates_left, rhotates_right,iotas,a_jagged, in_0, inlen,rate,M.leakages} /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224 /\ 
    to_uint a_jagged{2} + 224 < W64.modulus /\ 0 < to_uint rate{2} < 200
   ==>
   ={M.leakages}).
proc.
wp; call(_: ={a_jagged,in_0,inlen,rate,M.leakages} /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224 /\ 
    to_uint a_jagged{2} + 224 < W64.modulus /\ 0 < to_uint rate{2} < 200 /\
    to_uint inlen{2} < to_uint rate{2} 
 ==> ={M.leakages}).
proc.
while(={i,M.leakages}).
by auto => />.
wp;while(={j,in_0,inlen,M.leakages,l}). 
by auto => />.

wp;while(={j,in_0,inlen8,M.leakages,a_jagged} /\ 
    to_uint inlen8{2} <= 25 /\ to_uint j{2} <= to_uint inlen8{2} /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224 /\ 
    to_uint a_jagged{2} + 224 < W64.modulus /\ to_uint rate{2} < 200). 
 auto => />.
rewrite /eq_reg /disj_reg. 
progress.
congr.
move : H4; rewrite ultE => *.
by apply H1;rewrite to_uintD_small;by smt(@W64).
by smt(@W64).

auto => />.
progress.
congr.
move : (H11); rewrite /eq_reg => *.
rewrite H14 => //=.
rewrite to_uintD_small. smt(@W64). smt(@W64).
move : (H11); rewrite /eq_reg => * //=.
congr.
congr.
congr.
rewrite H16 => //=.

split; last first.
have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).

rewrite to_uintD_small.
have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
by smt(@W64).

move : (H11); rewrite /eq_reg => * //=.
congr.
congr.
congr.
rewrite H16 => //=.

split; last first.
have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).

rewrite shr_div => />.
by smt(@W64).
by smt(@W64).

rewrite to_uintD_small.
have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
by smt(@W64).

inline *.
unroll for {1} 8.
unroll for {2} 8.

auto => />.
progress.
rewrite shr_div => />.
smt(@W64).
rewrite shr_div => />.
smt(@W64).

move : (H); rewrite /eq_reg => * //=.
congr.
rewrite H7 => //=.

rewrite to_uintD_small.
by smt(@W64).
by smt(@W64).

congr.
move : (H); rewrite /eq_reg => * //=.
congr.
rewrite H8 => //=.

split.
rewrite to_uintD_small.
have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 224).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
by smt(@W64).


progress.

have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).

congr.
move : (H); rewrite /eq_reg => * //=.
congr.
rewrite H8 => //=.

split.
rewrite to_uintD_small.
have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
by smt(@W64).


progress.

have hh : (to_uint ((of_int 8)%W64 * (rate{2} - W64.one `>>` (of_int 3)%W8)) < 216).
rewrite to_uintM_small. 

have hh: (to_uint (rate{2} - W64.one `>>` (of_int 3)%W8) < 200).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).
rewrite shr_div => />.
by smt(@W64).
by smt(@W64).

wp; while (={M.leakages,rate,inlen,a_jagged,in_0,rhotates_left,rhotates_right,iotas} /\ 
    to_uint a_jagged{2} + 224 < W64.modulus /\ 0 < to_uint rate{2} < 200 /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224).
wp;call(_: ={M.leakages,_rhotates_left,_rhotates_right,_iotas} ==> ={M.leakages}).
proc.
by sim.
exists* rate{2}, inlen{2}.
elim* => rt il.
wp;call(_: ={M.leakages,rate,inlen,a_jagged,in_0} /\ 
    rate{2} = rt /\ inlen{2} = il /\
    to_uint a_jagged{2} + 224 < W64.modulus /\ 0 < to_uint rate{2} < 200 /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224 ==>
       res{1}.`3 = res{2}.`3 /\ res{1}.`4 = res{2}.`4 /\ ={M.leakages} /\
       res{1}.`4 = il - rt).
proc.
wp; while (={M.leakages,i}).
by auto => />.
wp; while (={j,rate8,M.leakages,a_jagged,in_0} /\ to_uint rate8{2} < 25 /\
    to_uint a_jagged{2} + 224 < W64.modulus /\ 0 < to_uint rate{2} < 200 /\
    eq_reg Glob.mem{1} Glob.mem{2} (to_uint a_jagged{1}) 224).
auto => />.
progress.
congr.
move : H3;rewrite /eq_reg => H3.
rewrite H3.
rewrite to_uintD_small => />.
smt(@W64).
smt(@W64).
rewrite to_uintD_small => />.
smt(@W64).

auto => />.
progress.
rewrite shr_div => />.
smt(@W64).

auto => />.
progress.
rewrite !uleE.
smt(@W64).
rewrite !uleE.
smt(@W64).
rewrite !uleE.
smt(@W64).

inline *. 
unroll for {1} 9.
unroll for {2} 9.
auto => />.
progress.
move : H3; rewrite uleE; smt(@W64).

inline *. 
unroll for {1} 10.
unroll for {2} 10.
by auto => />.
qed.
