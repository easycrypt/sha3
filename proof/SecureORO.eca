require import Int Distr Real FMap FSet Mu_mem.
require (*--*) StdOrder.
(*---*) import StdOrder.IntOrder.
require (****) PROM FelTactic.


type from, to.

op sampleto : to distr.

op bound : int.
axiom bound_ge0 : 0 <= bound.

axiom sampleto_ll:   is_lossless sampleto.
axiom sampleto_full: is_full     sampleto.
axiom sampleto_fu:   is_funiform sampleto.

clone import PROM.FullRO as RO with
  type in_t    <- from,
  type out_t   <- to,
  op   dout _  <- sampleto,
  type d_in_t  <- unit,
  type d_out_t <- bool.
import FullEager.

op increase_counter (c : int) (m : from) : int.
axiom increase_counter_spec c m : c <= increase_counter c m.

module type RF = {
  proc init() : unit
  proc get(x : from) : to option
  proc sample (x: from) : unit
}.

module RF (R : RO) : RF = {
  proc init = R.init
  proc get (x : from) : to option = {
    var y;
    y <@ R.get(x);
    return Some y;
  }
  proc sample = R.sample
}.

module Bounder (F : RF) : RF = {
  var bounder : int
  proc init () : unit = {
    bounder <- 0;
    F.init();
  }
  proc get(x : from) : to option = {
    var y : to option <- None;
    if (bounder < bound) {
      bounder <- bounder + 1;
      y <@ F.get(x);
    }
    return y;
  }
  proc sample = F.sample
}.


module type Oracle = {
  proc get(x : from) : to option {}
}.

module type AdvPreimage (F : Oracle) = {
  proc guess(h : to) : from
}.

module Preimage (A : AdvPreimage, F : RF) = {
  proc main () : bool = {
    var m,hash,hash';
    hash <$ sampleto;
    Bounder(F).init();
    m <@ A(Bounder(F)).guess(hash);
    hash' <@ Bounder(F).get(m);
    return hash' = Some hash;
  }
}.

section Preimage.

  declare module A <: AdvPreimage {-RO, -Preimage}.

  local module FEL (A : AdvPreimage, F : RF) = {
    proc main (hash : to) : from = {
      var m;
      Bounder(F).init();
      m <@ A(Bounder(F)).guess(hash);
      return m;
    }
  }.

  local module Preimage2 (A : AdvPreimage, F : RF) = {
    var hash : to
    proc main () : bool = {
      var m,hash';
      hash <$ sampleto;
      m <@ FEL(A,F).main(hash);
      hash' <@ Bounder(F).get(m);
      return hash' = Some hash;
    }
  }.

  lemma RO_is_preimage_resistant &m :
    Pr [ Preimage(A,RF(RO)).main() @ &m : res ] <= (bound + 1)%r * mu1 sampleto witness.
  proof.
  have->: Pr [ Preimage (A,RF(RO)).main() @ &m : res ] = 
         Pr [ Preimage2(A,RF(RO)).main() @ &m : res ].
  + by byequiv=> //=; proc; inline*; sim.
  byphoare(: _ ==> _) => //=; proc.
  seq 2 : (rng RO.m Preimage2.hash) (bound%r * mu1 sampleto witness) 1%r 1%r 
         (mu1 sampleto witness)
         (card (fdom RO.m) <= Bounder.bounder <= bound).
  + inline*; auto; call(: card (fdom RO.m) <= Bounder.bounder <= bound)=> //=.
    - proc; inline*; auto; sp; if; 2:auto; wp.
      wp; conseq(:_==> card (fdom RO.m) + 1 <= Bounder.bounder <= bound); 2: by auto;smt().
      move=> &h /> c H1 H2 c2 r x h1 h2; split; 2: smt().
      by rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
    by auto=> />; rewrite fdom0 fcards0; smt(bound_ge0).
  + seq 1 : true 1%r (bound%r * mu1 sampleto witness) 0%r _; auto.
    exists * Preimage2.hash; elim* => h.
    call(: Preimage2.hash = h /\ h = arg ==> rng RO.m h)=> //.
    bypr=> /> {&m} &m <<- <-.
    pose h := Preimage2.hash{m}.
    have H: forall &m h, 
         Pr[FEL(A, RF(RO)).main(h) @ &m : rng RO.m h] <= bound%r * mu1 sampleto witness; last first.
    + exact(H &m h).
    move=> {&m h} &m h.
    fel 1 Bounder.bounder (fun _, mu1 sampleto witness) bound (rng RO.m h) 
      [Bounder(RF(RO)).get: (card (fdom RO.m) <= Bounder.bounder < bound)]
      (card (fdom RO.m) <= Bounder.bounder <= bound)
      =>//.
    - rewrite StdBigop.Bigreal.BRA.big_const List.count_predT List.Range.size_range.
      rewrite ler_maxr //=; 1:smt(bound_ge0).
      rewrite-RField.AddMonoid.iteropE-RField.intmulpE; 1: smt(bound_ge0).
      by rewrite RField.intmulr; smt(). 
    - inline*; auto=> />.
      rewrite mem_rng_empty /= fdom0 fcards0 /=; smt(bound_ge0). 
    - proc.
      sp; if; auto; sp; inline*; sp; wp=> /=.
      case: (x \in RO.m); wp => //=.
      + by hoare; auto; smt(mu_bounded). 
      rnd (pred1 h); auto=> /> &h c H0 H1 H2 H3 H4 H5.
      rewrite (sampleto_fu h witness) /= => ? ?.
      rewrite rngE/= => [][] a; rewrite get_setE. 
      case: (a=x{h}) => [->>|] //=.
      by move:H2; rewrite rngE /= negb_exists/= => /(_ a) //=.
    - move=> c; proc; inline*; sp; if; sp.
      + auto; progress. 
        + smt().
        + by rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
        + smt().
        + smt().
        + smt().
        + smt().
      by auto.
    move=> b c; proc; sp; inline*; if; auto; progress. 
    - rewrite 2!rngE /= eq_iff; split=> [][] a.
      + by rewrite get_setE; move: H4; rewrite domE /=; smt().
      move=> H8; exists a; rewrite get_setE; move: H4; rewrite domE /=; smt().
    - smt().
    - by rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
    - smt().
    - smt().
    - smt().
    - smt().
  + by inline*; auto.
  + by inline*; auto.
  + inline*; sp; wp.
    if; sp; wp; last by hoare;auto;progress; smt(mu_bounded).
    case: (x \in RO.m).
    - hoare; auto; progress. 
      rewrite H3/=; move: H1; rewrite rngE /= negb_exists /=.
      by have:=H3; rewrite domE; smt().
    rnd (pred1 Preimage2.hash); auto=> /> &hr *.
    rewrite (sampleto_fu Preimage2.hash{hr} witness)/= => ??.
    by rewrite get_setE /=; smt().
  smt().
  qed.

end section Preimage.

(*-------------------------------------------------------------------------*)
module type AdvSecondPreimage (F : Oracle) = {
  proc guess(m : from) : from
}.

module SecondPreimage (A : AdvSecondPreimage, F : RF) = {
  proc main (m1 : from) : bool = {
    var m2, hash1, hash2;
    Bounder(F).init();
    m2 <@ A(Bounder(F)).guess(m1);
    hash1 <@ Bounder(F).get(m1);
    hash2 <@ Bounder(F).get(m2);
    return m1 <> m2 /\ exists y, Some y = hash1 /\ Some y = hash2;
  }
}.

section SecondPreimage.


  declare module A <: AdvSecondPreimage {-Bounder, -RO, -FRO}.
    
  local module FEL (A : AdvSecondPreimage, F : RF) = {
    proc main (m1 : from) : from = {
      var m2;
      Bounder(F).init();
      Bounder(F).sample(m1);
      m2 <@ A(Bounder(F)).guess(m1);
      return m2;
    }
  }.

  local module SecondPreimage2 (A : AdvSecondPreimage, F : RF) = {
    var m2 : from
    proc main (m1 : from) : bool = {
      var hash1,hash2;
      m2 <@ FEL(A,F).main(m1);
      hash1 <@ Bounder(F).get(m1);
      hash2 <@ Bounder(F).get(m2);
      return m1 <> m2 /\ exists y, Some y = hash1 /\ Some y = hash2;
    }
  }.

  local module D1 (A : AdvSecondPreimage, F : RO) = {
    var m1 : from
    proc distinguish () : bool = {
      var b;
      b <@ SecondPreimage2(A,RF(F)).main(m1);
      return b;
    }
  }.

  local module SecondPreimage3 (A : AdvSecondPreimage, F : RO) = {
    proc main (m1 : from) : bool = {
      var b;
      SecondPreimage2.m2 <- witness;
      D1.m1 <- m1;
      Bounder(RF(F)).init();
      b <@ D1(A,F).distinguish();
      return b;
    }
  }.
      

  lemma RO_is_second_preimage_resistant &m (mess1 : from) :
    Pr [ SecondPreimage(A,RF(RO)).main(mess1) @ &m : res ]
      <= (bound + 1)%r * mu1 sampleto witness.
  proof.
  have->: Pr [ SecondPreimage(A,RF(RO)).main(mess1) @ &m : res ] =
          Pr [ SecondPreimage(A,RF(LRO)).main(mess1) @ &m : res ].
  + by byequiv=> //=; proc; inline*; sim.
  have->: Pr [ SecondPreimage(A,RF(LRO)).main(mess1) @ &m : res ] =
          Pr [ SecondPreimage2(A,RF(LRO)).main(mess1) @ &m : res ].
  + by byequiv=> //=; proc; inline*; sim.
  have->: Pr [ SecondPreimage2(A,RF(LRO)).main(mess1) @ &m : res ] =
          Pr [ SecondPreimage2(A,RF(RO)).main(mess1) @ &m : res ].
  + have->: Pr [ SecondPreimage2(A,RF(LRO)).main(mess1) @ &m : res ] =
            Pr [ SecondPreimage3(A,LRO).main(mess1) @ &m : res ].
    - by byequiv=> //=; proc; inline*; wp 15 -2; sim.
    have->: Pr [ SecondPreimage3(A,LRO).main(mess1) @ &m : res ] =
            Pr [ SecondPreimage3(A,RO).main(mess1) @ &m : res ].
    - rewrite eq_sym.
      byequiv=>//=; proc.
      call(RO_LRO_D (D1(A)) _); first by rewrite sampleto_ll.
      by inline*; auto.
    by byequiv=> //=; proc; inline*; wp -2 18; sim.
  byphoare(: arg = mess1 ==> _)=>//=; proc.
  seq 1 : (rng (rem RO.m mess1) (oget RO.m.[mess1]))
          (bound%r * mu1 sampleto witness) 1%r
          1%r (mu1 sampleto witness)
          (card (fdom RO.m) - 1 <= Bounder.bounder <= bound
            /\ mess1 \in RO.m /\ mess1 = m1).
  + inline*; auto; call(: card (fdom RO.m) - 1 <= Bounder.bounder <= bound
            /\ mess1 \in RO.m).
    - proc; inline*; auto; sp; if; last by auto; smt().
      auto=> /> &h c Hc Hdom Hc2 sample.
      by rewrite sampleto_full/=!fdom_set !fcardU !fcard1;smt(mem_set fcard_ge0).
    auto=> /> sample.
    by rewrite mem_set mem_empty/= fdom_set fdom0 fset0U fcard1; smt(bound_ge0).
  + call(: arg = mess1 ==> rng (rem RO.m mess1) (oget RO.m.[mess1])); auto.
    bypr=> {&m} &m h; rewrite h.
    fel 2 Bounder.bounder (fun _, mu1 sampleto witness) bound 
        (mess1 \in RO.m /\ rng (rem RO.m mess1) (oget RO.m.[mess1]))
        [Bounder(RF(RO)).get: (card (fdom RO.m) - 1 <= Bounder.bounder < bound)]
        (card (fdom RO.m) - 1 <= Bounder.bounder <= bound /\ mess1 \in RO.m)=> {h}
      =>//.
    + rewrite StdBigop.Bigreal.BRA.big_const List.count_predT List.Range.size_range.
      rewrite ler_maxr //=; 1:smt(bound_ge0).
      rewrite-RField.AddMonoid.iteropE-RField.intmulpE; 1: smt(bound_ge0).
      by rewrite RField.intmulr; smt(mu_bounded bound_ge0). 
    + inline*; auto=> />.
      move=> r; rewrite mem_empty /= !mem_set mem_empty/= sampleto_full /=.
      rewrite get_set_sameE//= fdom_set fdom0 fset0U fcard1 /= rngE /=; split; 2: smt(bound_ge0). 
      by rewrite negb_exists/= => a; rewrite remE get_setE //= emptyE; smt().
    + proc; inline*; sp; if; last by hoare; auto.
      sp; case: (x \in RO.m)=> //=.
      - by hoare; auto; smt(mu_bounded).
      rcondt 2; 1: auto; wp=> /=. 
      conseq(:_ ==> pred1 (oget RO.m.[mess1]) r)=> />. 
      - move=> /> &h c H0c Hcb Hnrng Hmc _ Hdom1 Hdom2 sample.
        rewrite mem_set Hdom1 /= get_set_neqE; 1: smt().
        have->: (rem RO.m{h}.[x{h} <- sample] mess1) = (rem RO.m{h} mess1).[x{h} <- sample].
        + by apply fmap_eqP=> y; rewrite remE 2!get_setE remE; smt().
        move: Hnrng; rewrite Hdom1 /= rngE /= negb_exists /= => Hnrng.
        rewrite rngE/= => [][] mess; rewrite get_setE remE.
        by have:= Hnrng mess; rewrite remE; smt().
      rnd; auto; progress. 
      by have ->:= sampleto_fu witness (oget RO.m{hr}.[mess1]).
    + move=> c; proc; inline*; sp; if; auto; progress. 
      - smt().
      - by rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
      - smt().
      - smt(mem_set).
      - smt().
      - smt().
      - smt().
    + move=> b c; proc; inline*; sp; if; auto; smt().
  + by inline*; auto.
  + by auto. 
  + inline*; sp; auto.
    if; sp; last first.  
    + sp; hoare; auto; if; auto.
    case(Bounder.bounder < bound); last first.
    - by rcondf 8; 1: auto; hoare; auto. 
    rcondt 8; 1: auto.
    swap 11 -8; sp.
    swap [7..11] -6; sp.
    swap[5..6] 2; wp 6=> /=.
    case: (SecondPreimage2.m2 \in RO.m).
    - rcondf 5; 1: auto; hoare; auto=> /> &h d _ _ in_dom1 not_rng d_bound _ in_dom2.
      move=> sample2 _ m1_in_RO sample1 _; rewrite negb_and/=.
      move: not_rng; rewrite rngE /= negb_exists /= => /(_ SecondPreimage2.m2{h}).
      rewrite remE; case: (SecondPreimage2.m2{h} = m1{h})=> //=.
      by move: in_dom1 in_dom2; smt().
    rcondt 5; 1: auto; wp. 
    rnd (pred1 (oget RO.m.[x3])); auto.
    move => /> &h d _ _ in_dom1 not_rng _ _ nin_dom2 sample2 _.
    rewrite (sampleto_fu (oget RO.m{h}.[m1{h}]) witness) /= => _ sample1 _.
    by rewrite get_set_sameE//=; smt().
  smt().
  qed.

end section SecondPreimage.


(*--------------------------------------------------------------------------*)
module type AdvCollision (F : Oracle) = {
  proc guess() : from * from
}.

module Collision (A : AdvCollision, F : RF) = {
  proc main () : bool = {
    var m1,m2,hash1,hash2;
    Bounder(F).init();
    (m1,m2) <@ A(Bounder(F)).guess();
    hash1 <@ Bounder(F).get(m1);
    hash2 <@ Bounder(F).get(m2);
    return m1 <> m2 /\ exists y, Some y = hash1 /\ Some y = hash2;
  }
}.

section Collision.

  declare module A <: AdvCollision {-RO, -FRO, -Bounder}.

  local module FEL (A : AdvCollision, F : RF) = {
    proc main () : from * from = {
      var m1,m2;
      Bounder(F).init();
      (m1,m2) <@ A(Bounder(F)).guess();
      return (m1,m2);
    }
  }.

  local module Collision2 (A : AdvCollision) (F : RF) = {
    proc main () : bool = {
      var m1,m2,hash1,hash2;
      (m1,m2) <@ FEL(A,F).main();
      hash1 <@ Bounder(F).get(m1);
      hash2 <@ Bounder(F).get(m2);
      return m1 <> m2 /\ exists y, Some y = hash1 /\ Some y = hash2;
    }
  }.

  op collision (m : ('a, 'b) fmap) =
    exists m1 m2, m1 <> m2 /\ m1 \in m /\ m2 \in m /\ m.[m1] = m.[m2].

  lemma RO_is_collision_resistant &m :
    Pr [ Collision(A,RF(RO)).main() @ &m : res ]
      <= ((bound * (bound - 1) + 2)%r / 2%r * mu1 sampleto witness).
  proof.
  have->: Pr [ Collision(A,RF(RO)).main() @ &m : res ] =
          Pr [ Collision2(A,RF(RO)).main() @ &m : res ].
  + by byequiv=>//=; proc; inline*; sim.
  byphoare=> //; proc.
  seq 1 : (collision RO.m)
      ((bound * (bound - 1))%r / 2%r * mu1 sampleto witness) 1%r
      1%r (mu1 sampleto witness)
      (card (fdom RO.m) <= Bounder.bounder <= bound); first last; first last.
  + auto.
  + auto.
  + inline*; sp.
    if; sp; last first.
    - by wp; conseq(:_==> false)=> />; hoare; auto. 
    case: (Bounder.bounder < bound); last first.
    - rcondf 8; 1:auto; hoare; auto.
    rcondt 8; 1: auto.
    swap 11 -8.
    swap [7..11] -6; sp.
    swap [5..6] 1; wp 5=> /=.
    swap 3 -1.
    case: (m1 = m2).
    - by hoare; auto. 
    case: (m1 \in RO.m); case: (m2 \in RO.m).
    - rcondf 3; 1: auto; rcondf 4; 1: auto; hoare; auto. 
      move=> /> &h d _ _ Hcoll _ _ neq12 in_dom1 in_dom2 _ _ _ _.
      move: Hcoll; rewrite /collision negb_exists /= => /(_ m1{h}).
      rewrite negb_exists /= => /(_ m2{h}).
      by rewrite neq12 in_dom1 in_dom2 /=; smt().
    - rcondf 3; 1: auto; rcondt 4; 1: auto; wp. 
      rnd (pred1 (oget RO.m.[x3])).
      auto=> /> &h d Hmc Hcb Hcoll _ _ neq12 in_dom1 in_dom2 _ _; split.
      * smt(sampleto_fu).
      by move=> _ sample _; rewrite get_set_sameE; smt().
    - rcondt 3; 1: auto; rcondf 5; 1: (by auto; smt(mem_set)).
      swap 1; wp=> /=; rnd (pred1 (oget RO.m.[m2])); auto.
      move=> /> &h d _ _ Hcoll _ _ neq12 in_dom1 in_dom2 _ _; split.
      * smt(sampleto_fu).
      move=> _ sample _.
      by rewrite get_set_sameE get_set_neqE 1:eq_sym.
    rcondt 3; 1: auto; rcondt 5; 1: (by auto; smt(mem_set)).
    swap 2 -1; swap 1; wp=> //=; rnd (pred1 r); auto.
    move=> /> &h d _ _ Hcoll _ _ neq12 in_dom1 in_dom2 sample1 _; split.
    * smt(sampleto_fu).
    move=> _ sample2 _.
    by rewrite get_set_sameE get_set_sameE; smt().
  + by move=> />; smt(mu_bounded).
  + inline*; wp; call(: card (fdom RO.m) <= Bounder.bounder <= bound); auto.
    - proc; inline*; sp; if; last by auto; smt().
      auto=> /> &h d Hbc Hcb sample _; split.
      * by move=> nin_dom1; rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
      by move=> in_dom1; smt().
    by move=> />; rewrite fdom0 fcards0; smt(bound_ge0).
  call(: true ==> collision RO.m); auto; bypr=> /> {&m} &m.
  fel 1 Bounder.bounder (fun i, i%r * mu1 sampleto witness) bound 
        (collision RO.m)
        [Bounder(RF(RO)).get: (card (fdom RO.m) <= Bounder.bounder < bound)]
        (card (fdom RO.m) <= Bounder.bounder <= bound)=> //. 
  + rewrite -StdBigop.Bigreal.BRA.mulr_suml RField.mulrAC.
    rewrite StdOrder.RealOrder.ler_wpmul2r; 1: smt(mu_bounded).
    by rewrite StdBigop.Bigreal.sumidE //; smt(bound_ge0).
  + inline*; auto=> />.
    rewrite fdom0 fcards0; split; 2: smt(bound_ge0). 
    rewrite /collision negb_exists /= => a; rewrite negb_exists /= => b.
    by rewrite mem_empty.
  + bypr=> /> {&m} &m; pose c := Bounder.bounder{m}; move=> H0c Hcbound Hcoll Hmc _.
    byphoare(: !collision RO.m /\ card (fdom RO.m) <= c ==> _)=>//=.
    proc; inline*; sp; if; last first.
    - by hoare; auto=> _ _; smt(le_fromint ge0_mu).
    case: (x \in RO.m).
    - by hoare; auto; smt(le_fromint mu_bounded).
    rcondt 5; 1: auto; sp; wp=> /=.
    conseq(:_==> r \in frng RO.m).
    - move=> /> &h c2 Hcoll2 Hb2c Hc2b nin_dom sample m1 m2 neq.
      rewrite 2!mem_set.
      case: (m1 = x{h}) => //=.
      * move=> <<-; rewrite eq_sym neq /= get_set_sameE get_set_neqE//= 1:eq_sym //.
        by rewrite mem_frng rngE /= => _ ->; exists m2.
      case: (m2 = x{h}) => //=.
      * move=> <<- _ in_dom1.
        by rewrite get_set_neqE // get_set_sameE mem_frng rngE/= => <-; exists m1.
      move=> neq2 neq1 in_dom1 in_dom2; rewrite get_set_neqE // get_set_neqE //.
      have:= Hcoll2; rewrite negb_exists /= => /(_ m1).
      rewrite negb_exists /= => /(_ m2).
      by rewrite neq in_dom1 in_dom2 /= => ->.
    rnd; skip=> /> &h bounder _ h _. 
    rewrite (mu_mem (frng RO.m{h}) sampleto (mu1 sampleto witness)); 1: smt(sampleto_fu).
    rewrite StdOrder.RealOrder.ler_wpmul2r //. 
    by rewrite le_fromint; smt(le_card_frng_fdom). 
  + move=> c; proc; inline*; auto; sp; if; last by auto; smt().
    auto=> /> &h h1 h2 _ sample _.
    by rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
  move=> b c; proc; inline*; sp; if; auto.
  move=> /> &h  h1 h2 _ h3 sample _.
  by rewrite fdom_set fcardU fcard1; smt(fcard_ge0).
  qed.


end section Collision.
