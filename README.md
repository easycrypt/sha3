# Machine-Checked Proofs for Cryptographic Standards

## Indifferentiability of Sponge and Secure High-Assurance Implementations of SHA-3

This repository contains the proof and implementation artefacts
associated with the paper "Machine-Checked Proofs for Cryptographic
Standards: Indifferentiability of Sponge and Secure High-Assurance
Implementations of SHA-3", which will appear at CCS 2019.

### Contents

The `proof` folder contains all EasyCrypt proofs, including:

* The root `proof` folder and the `proof/smart_counter` folder contain generic
  definitions, proofs of indifferentiability for Sponge, and proofs of security
  for SHA-3 and SHAKE;

* The `proof/implem` folder contains all proofs pertaining to the Jasmin
  implementation, and some related libraries:

  - The root `proof/implem` folder contains proofs of equivalence between the
    Jasmin implementations of the Sponge and its EasyCrypt specification, and
    proofs that the Jasmin implementations, instantiated with the corresponding
    implementations of the permutation, are leak-free in the cryptographic
    constant-time model;

  - The `proof/implem/perm` folder contains proofs of correctness for the
    permutation, and proofs of equivalence between its reference and optimized
    implementations.

### Checking the proofs

The proof of indifferentiability and random permutation-security for
Sponge and SHA-3 can be checked using EasyCrypt 1.x without any
additional dependencies.

NOTE: Jasmin and EasyCrypt have diverged and this repository has not been kept
up-to-date. Up-to-date versions of the Jasmin-related code and proofs can be
found in the [libjc repository][libjc]. The below can be used to check the
proofs as they were at the time of publication, taking care to use the correct
version of EasyCrypt.

[libjc]: https://github.com/tfaoliveira/libjc

The proofs of correctness for Jasmin also relies on Jasmin's `eclibs`
(currently at commit [29b97ec1][jasmin]).

[jasmin]: https://github.com/jasmin-lang/jasmin/tree/29b97ec1bc4855651a7a5d4c13a4397a9f84f944

You can fetch it using:

```base
git submodule update --init
```

From the root of this (`sha3`) repository, you can now run the following command
to check all Jasmin-related targets.

```bash
make check jsponge jperm libc
```

### Compiling from Jasmin to ASM

TODO

